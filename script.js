const GRID_SIZE = 3;
let isPlayerOne = true;
let className = 'player1';
let cells;
window.onload = () => {
  cells = [...document.getElementsByClassName('cell')];
  cells.forEach((cell, indexCell) => {
    cell.index = indexCell;
    cell.addEventListener('click', () => {
      if (isFreeCell(cell)) {
        cell.className += ` ${className}`;
        testHaveWon(cell);
        switchPlayer();
      } else {
        alert('can\'t place here');
      }

    });
  });
};

function isFreeCell(cell) {
  return !cell.className.split(' ').find(cl => cl === 'player1' || cl === 'player2');
}

function isPlayerCell(x, y) {
  return !!cells[x + y * GRID_SIZE].className.split(' ').find(cl => cl === className);
}

function switchPlayer() {
  if (isPlayerOne) {
    className = 'player2';
  } else {
    className = 'player1';
  }
  isPlayerOne = !isPlayerOne;
}

function testHaveWon(cell) {
  let canWin = false;
  let x = cell.index % GRID_SIZE,
    y = Math.trunc(cell.index / GRID_SIZE);
  if (!canWin) {
    let countCol = 0;
    let countRow = 0;
    let indexRight = 1,
      indexLeft = 1,
      indexTop = 1,
      indexBottom = 1;
    while (isInGrid(x - indexLeft, y) && isPlayerCell(x - indexLeft, y)) {
      countRow++;
      indexLeft++;
    }
    while (isInGrid(x + indexRight, y) && isPlayerCell(x + indexRight, y)) {
      countRow++;
      indexRight++;
    }
    while (isInGrid(x, y - indexTop) && isPlayerCell(x, y - indexTop)) {
      countCol++;
      indexTop++;
    }
    while (isInGrid(x, y + indexBottom) && isPlayerCell(x, y + indexBottom)) {
      countCol++;
      indexBottom++;
    }
    if (countCol >= 2 || countRow >= 2) {
      canWin = true;
    }
  }
  if (canWin) {

    cells.forEach(c => {
      c.className += ' test';
    });
    document.getElementById('info').innerText = `${className} wins!!!`;
  } else {
    let freeCell = false;
    cells.forEach(c => {
      if (isFreeCell(c)) {
        freeCell = true;
      }
    });
    if (!freeCell) {
      document.getElementById('info').innerText = 'No more move. Nobody win!';
    }
  }
}

function isInGrid(x, y) {
  return x >= 0 && x < GRID_SIZE && y >= 0 && y < GRID_SIZE;
}
